Collections Comparison:

Linked	-	Insertion Order is maintained
Hash	-	No Ordering, Duplicates allowed
Set	-	No Order, No Duplicates
Tree	-	Elements are sorted.