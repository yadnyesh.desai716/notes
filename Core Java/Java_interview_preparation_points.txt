Interview preparation points
  Java -->
	-	What is JDK, JRE, JVM? [JVM contains JIT which converts the bytecode into machine code]
	-	Features of Java [Inheritance, Polymorphism, Abstraction, Encapsulation, Object oriented approach]
	-	Java supports 2 types of Polymorphisms  1. Compile-Time Polymorphism/Method Overloading &
												2. Run-Time Polymorphism/Method Overridding
	-	Exceptions in java								
	-	Checked & unchecked exceptions in java
	-	Exception handling & Throwable				
	-	Collections framework
	-	Abstract class vs Interface
	-	Array vs ArrayList vs LinkedList
	-	Map VS Set										
	-	Single vs Multiple vs Multi-level Inheritance
	-	String VS StringBuffer VS StringBuilder VS String pool
	-	Comparable vs Comparator
	-	Iterable VS Iterator
	-	Memory allocation using Stack & Heap memories.	Link: https://www.baeldung.com/java-stack-heap
	-	Collector VS Collectors							<--------- Learn
	-	Collection VS Collections						<--------- Learn
	-	Singleton & Other design patterns				<--------- Learn
	-	Multithreading									
	-	Debugging & Unit Testing						<--------- Learn
	-	Databases										<--------- Learn				
	-	Garbage collector in Java						
	-	Explain final, finally, finalize in Java		
	-	JDK 8 new features
			-	Lambda expressions
			-	Method references
			-	Funtional Interfaces [Comparable, Runnable, Consumer[void accept(T t)], Funtion[R apply(T t)], Predicate[boolean test(T t)],
										Supplier[T get()]
			-	Stream API
			-	default, static & private methods in interfaces
			-	Optional Class [This is a "final" class so cannot be inherited i.e. an immutable class] Some abstract methods of stream interface
				have "Optional<T>" return type
	-	coding practice [primeNumbers, fibonacci, QuickSort, BinarySearch, find occurrences & many other]			<--------- Practice & Learn
	-	LeetCode Problem Solving Practice.
	
  AWS -->
	-	Benefits of cloud
	-	6 pillars
	-	Compute services
	-	Strorage services
	-	Networking services
	-	Security & access management services
	-	Serverless services
	
  Read Resume