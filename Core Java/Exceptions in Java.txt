Exceptions in Java:
	-	The core advantage of exception handling is to maintain the normal flow of the application.
	-	Two approaches to handle errors or exceptions in programming
		1.	LBYL	(Look Before You Leap)
		2.	EAFP	(Easy to Ask for Forgiveness than Permission)
		
		e.g.
			public static Integer lbyl(int x, int y) {		// <--- returning "Auto-boxed" Integer value
				if ( y !=0 )
					return x / y;
				else
					return 0;
			}
			
			public static Integer eafp(int x, int y) {
				try {
					return x / y;
				} catch(ArithmaticException e) {
					return 0;
				}
				
	-	Types of Exceptions:
			1.	Checked Exceptions		<--- Checked at Compile-time.
			2.	Unchecked Exceptions	<--- Checked at Runtime.
			3.	Errors					<--- An event which is irrecoverable.
	-	Every subclass of Error and RuntimeException is an unchecked exception in Java.
	-	A checked exception is everything else under the Throwable class, e.g. SQLException, IOException, ClassNotFoundException, etc.
	-	Exceptions in Java can be handled by 5 keywords,
			1.	try block		<--	Try block must be followed by either catch or finally block.
			2.	catch block		<--	Multiple catch blocks are possible but only one will be invoked at a time.
								<--	All catch blocks must be ordered from most specific to most general exception.
			3.	finally block	<--	It gets invoked inspite of whether exception is handled or not. Only one finally block can be specified with one try block.
			4.	throw keyword	<--	It is used to throw an exception.
			5.	throws keyword	<--	It is used to declare exceptions. It doesn't throw any exception. It is always used with method signature.
			
	Q.	Which exceptions should be declared using "throws" keyword?
	Ans.	Checked exceptions only because,
				-	unchecked exceptions are under our control so we can correct our code.
				-	error: beyond our control. For example, we are unable to do anything if there occurs VirtualMachineError or StackOverflowError.
	
	Q.	Can we rethrow an exception?
	Ans.	Yes, by throwing same exception in catch block.
	
	-	Rules of Exception Handling with Method Overriding:
			1.	If the superclass method does not declare an exception, subclass overridden method cannot declare the checked exception but it can
				declare unchecked exception.
			
			2.	If the superclass method declares an exception, subclass overridden method can declare same, subclass exception or no exception but
				cannot declare parent exception.
			