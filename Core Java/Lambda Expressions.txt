Lambda Expressions:
	-	An instance is created when we use Anonymous class but no instance is created when we use Lambda.
	-	Lambda expressions are treated as inner code blocks.
			e.g.
				{	// Inner code block starts here.
					....
					....
					// Lambda expression with its own block of code.
					....
					....
				}	// Inner code block ends here.
	-	A lambda expression can be assigned to a variable and this variable can then be used as of when needed. Benefit of doing this is the lambda
			expression need to be declared only once and the assigned variable can be used multiple times.
	-	Local variables can be used in Lambda expressions if and only if those variables are final or effectively final, i.e. their value is not changed.
		
		Q.	Why local variables need to be final or effectively final when using in Lambda expressions?
		-->	If local variables are not final or effectively final then the runtime will have no way of knowing what values to use when the lambda expression is evaluated.
			In other words, lambda expressions in Java may not be immediately evaluated so any variable used in the lambda, from outside the lambda, must be final or effectively final.
				
	-	You can choose to avoid using Lambda expressions altogether. Instead you can always use Anonymous class. i.e. using the Interface's implementation directly.
	