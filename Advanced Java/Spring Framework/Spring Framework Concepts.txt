Spring Framework Concepts:

	-	Coupling:	Measures how much change is involved in changing something in the application.
					Java Interfaces and Spring framework helps in achieving Loose Coupling.
	-	When a Java application is launched, an instance of JVM is created.
	-	In this JVM instance we need to launch a Spring Context.
	-	To launch a Spring Context, we need to specify a Configuration file.
	-	This Configuration file contains the Bean methods.
	-	We can declare a class as a Configuration file by annotating it with @Configuration annotation.
	-	@Configuration Annotation:	Indicates that a class declares one or more @Bean methods and may be processed by the Spring
		Container to generate bean definitions and service requests for those beans at runtime.
			-	Spring @Configuration annotation is part of the spring core framework.
			-	Spring @Configuration annotation indicates that the class has @Bean definition methods.
			-	So Spring container can process the class and generate Spring Beans to be used in the application.
			
		//  Q. How to get beans from Spring Context?
        //  Ans. Ask the Spring Context to provide the beans using "context.getBean()" method.
        e.g.	System.out.println(context.getBean("firstName"));
		
	-	Spring Container --> Manages Spring beans and their life-cycle
			Two Types:	1.	Bean Factory	-	Basic Spring Container. (Only used in applications with strict memory constraints, such as IoT applications.)
			
						2.	Application Context	-	Advanced Spring Container with enterprise specific features,
							(Most frequently used)					-	Easy to use in web applications
							e.g. web apps, web servies,				-	Easy internationalization
								microservices						-	Easy integration with Spring AOP (Aspect Oriented Programming)

	Q.	What is the difference between Java Bean, POJO & Spring Bean?
	-	Java Bean --> Classes adhering to 3 constraints:
						1.	have public default (no argument) constructor
						2.	allow access to their fields by using getters and setters methods
						3.	implements java.io.Serializable
	-	POJO --> Plain Old Java Object
					-	No constraints
					-	Any Java Object is a POJO
	-	Spring Bean -->	Any Java Object that is managed by Spring
							-	Spring uses IOC Container (Bean Factory or Application Context) to manage these objects.
							-	This IOC Container is also referred to as Spring Container.
	-	Spring Framework Bean Autowiring Multiple Bean candidate conflict resolution -->	can be done using @Primary & @Qualifier() annotations.
		1.	@Primary Annotation:
				-	Denotes that this bean is a primary bean among all the beans of the same type.
				-	Indicates that a bean should be given preference when multiple candidates are qualified to autowire a single-valued dependency.
				e.g.	In the following example, 2 Person beans are present. @Primary is used to auto-wire one of them as the Primary bean.
							@Bean
							@Primary
							public Person person() {
								return new Person("Joey", 30, new Address("New York", "NY"));
							}
							
							@Bean(name = "addressViaMethodCall")
							public Person person1() {
								return new Person(name(), age(), address());
							}
							
		2.	@Qualifier() Annotation:	This annotation denotes that its associated Bean is being used somewhere in the code i.e. this Bean
										is being auto-wired.
				e.g. In the following example, "address2" Bean is annotated with @Qualifier("bengaluruAddr") which is being auto-wired in the
					"person2" Bean.
							@Bean(name = "address2")
							@Qualifier("bengaluruAddr")
							public Address address2() {
								return new Address("Bengaluru", "KA");
							}

							@Bean(name = "usingParameters")
							public Person person2(String name, int age, @Qualifier("bengaluruAddr") Address address2) {
								return new Person("BengaluruGuy", age, address2);
							}
							
	-	@Qualifier is having higher priority than @Primary.
	-	@Component:	An instance of a class specified with @Component annotation will be created. i.e. Spring framework will create the Beans
					automatically
	-	Print total number of Beans:	System.out.println(context.getBeanDefinitionCount());
	-	Print all the Beans:	Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);

	-	Types of Dependency Injection:
		1.	Constructor-based:	Dependencies are set by creating the Bean using its Constructor
		2.	Setter-based:	Dependencies are set by calling setter methods on your beans
		3.	Field:	No setter or constructor, Dependency is injected using reflection.
		
		Q.	Which type of Dependency Injection should be used and why?
		Ans.	Constructor Injection should be used because all the initialization happens in one method.
				Once the initialization is done, the Bean is ready for use.
	-	@Component:	An instacne of class will be managed by Spring Framework.
	-	@ComponentScan:	Tell Spring Framework where to find Component classes.
	-	Autowiring: The process of wiring in the dependencies for a Spring Bean.
	-	Dependency Injection:	Identify beans, their dependencies and wire them together (provides IOC - Inversion of Control)
	-	@Component recommended for instantiating beans for your own application code.
	-	@Bean recommended for instantiating Beans for 3rd-party libraries - for example, Spring Security configuration.
	Q.	How does Spring Framework find component classes?
	Ans.	Spring framework scans packages for component classes - e.g. `@ComponentScan("com.in28minutes")`
	
	***	Spring Framework contains multiple Spring Modules: Spring core, Spring MVC, Spring JDBC, Spring test etc..
	***	 Spring Projects - Spring Security, Spring Data, Spring Boot and Spring Cloud etc...