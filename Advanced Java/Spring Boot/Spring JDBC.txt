Spring JDBC:

	Q.	What is Spring JdbcTemplate?
   -->	Spring JdbcTemplate is a powerful mechanism to connect to the database and execute SQL queries. It internally uses JDBC api, but eliminates a
		lot of problems of JDBC API.	
		
		The problems of JDBC API are as follows:

		-	We need to write a lot of code before and after executing the query, such as creating connection, statement, closing resultset,
			connection etc.
		-	We need to perform exception handling code on the database logic.
		-	We need to handle transaction.
		-	Repetition of all these codes from one to another database logic is a time consuming task.
		
		Advantage of Spring JdbcTemplate:
			Spring JdbcTemplate eliminates all the above mentioned problems of JDBC API. It provides you methods to write the queries directly, so it saves a lot of work and time.

	-	Things you did while learning Spring JDBC:
		-	Create a file named "schema.sql" which contains CREAT TABLE query.
		-	Use H2 database and enable its console. (Refer H2 Database file)
		-	Create a class that implements "CommandLineRunner" interface and its abstract method "run()".
		-	Define a class as @Repository in which SQL queries are defined and methods that use these queries are defined.
				In the repository make use of "JdbcTemplate" class that contains various methods such as,
					execute()	-	used to execute DDL queries.
					update()	-	for insert, update and delete records. 
					query()		-	for select queries on tables.
					queryForList(), queryForMap(), queryForObject(), queryForRowSet(), queryForStream()
		-	Make use of "BeanPropertyRowMapper" class: RowMapper implementation that converts a row into a new instance of the specified mapped
			target class. The mapped target class must be a top-level class or static nested class, and it must have a default or no-arg constructor.
	-	Using Spring JDBC makes it easy to write queries in Java but with more number of tables it will become complex over the time so opt for
		Spring JPA.
